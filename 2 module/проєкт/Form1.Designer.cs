﻿namespace Shop
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.idtext = new System.Windows.Forms.TextBox();
			this.namecombo = new System.Windows.Forms.ComboBox();
			this.typecombo = new System.Windows.Forms.ComboBox();
			this.quanitycombo = new System.Windows.Forms.ComboBox();
			this.paytext = new System.Windows.Forms.TextBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.namecoffee = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.type = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.quanity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.pay = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(398, 25);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(254, 51);
			this.label1.TabIndex = 0;
			this.label1.Text = "Coffee shop";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(68, 142);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(49, 32);
			this.label2.TabIndex = 1;
			this.label2.Text = "ID:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.Location = new System.Drawing.Point(68, 227);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(188, 32);
			this.label3.TabIndex = 2;
			this.label3.Text = "Name Coffee:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label4.Location = new System.Drawing.Point(68, 312);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(85, 32);
			this.label4.TabIndex = 3;
			this.label4.Text = "Type:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label5.Location = new System.Drawing.Point(68, 401);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(121, 32);
			this.label5.TabIndex = 4;
			this.label5.Text = "Quanity:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label6.Location = new System.Drawing.Point(68, 484);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(71, 32);
			this.label6.TabIndex = 5;
			this.label6.Text = "Pay:";
			// 
			// idtext
			// 
			this.idtext.Location = new System.Drawing.Point(272, 142);
			this.idtext.Name = "idtext";
			this.idtext.Size = new System.Drawing.Size(181, 22);
			this.idtext.TabIndex = 6;
			this.idtext.TextChanged += new System.EventHandler(this.idtext_TextChanged);
			this.idtext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.idtext_KeyPress);
			// 
			// namecombo
			// 
			this.namecombo.FormattingEnabled = true;
			this.namecombo.Items.AddRange(new object[] {
			"Latte",
			"Chocolatte",
			"Capuccinno ",
			"Coffee",
			"Tea"});
			this.namecombo.Location = new System.Drawing.Point(272, 227);
			this.namecombo.Name = "namecombo";
			this.namecombo.Size = new System.Drawing.Size(181, 24);
			this.namecombo.TabIndex = 7;
			// 
			// typecombo
			// 
			this.typecombo.FormattingEnabled = true;
			this.typecombo.Items.AddRange(new object[] {
			"Ice",
			"Hot"});
			this.typecombo.Location = new System.Drawing.Point(272, 320);
			this.typecombo.Name = "typecombo";
			this.typecombo.Size = new System.Drawing.Size(181, 24);
			this.typecombo.TabIndex = 8;
			// 
			// quanitycombo
			// 
			this.quanitycombo.FormattingEnabled = true;
			this.quanitycombo.Items.AddRange(new object[] {
			"1",
			"2",
			"3",
			"4",
			"5",
			"6",
			"7",
			"8",
			"9",
			"10"});
			this.quanitycombo.Location = new System.Drawing.Point(272, 401);
			this.quanitycombo.Name = "quanitycombo";
			this.quanitycombo.Size = new System.Drawing.Size(174, 24);
			this.quanitycombo.TabIndex = 9;
			// 
			// paytext
			// 
			this.paytext.ForeColor = System.Drawing.SystemColors.Info;
			this.paytext.Location = new System.Drawing.Point(272, 494);
			this.paytext.Name = "paytext";
			this.paytext.Size = new System.Drawing.Size(193, 22);
			this.paytext.TabIndex = 10;
			// 
			// dataGridView1
			// 
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.id,
			this.namecoffee,
			this.type,
			this.quanity,
			this.pay});
			this.dataGridView1.Location = new System.Drawing.Point(499, 102);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowHeadersWidth = 51;
			this.dataGridView1.RowTemplate.Height = 24;
			this.dataGridView1.Size = new System.Drawing.Size(656, 368);
			this.dataGridView1.TabIndex = 11;
			// 
			// id
			// 
			this.id.HeaderText = "ID";
			this.id.MinimumWidth = 6;
			this.id.Name = "id";
			this.id.Width = 125;
			// 
			// namecoffee
			// 
			this.namecoffee.HeaderText = "Name Coffee";
			this.namecoffee.MinimumWidth = 6;
			this.namecoffee.Name = "namecoffee";
			this.namecoffee.Width = 125;
			// 
			// type
			// 
			this.type.HeaderText = "Type";
			this.type.MinimumWidth = 6;
			this.type.Name = "type";
			this.type.Width = 125;
			// 
			// quanity
			// 
			this.quanity.HeaderText = "Quanity";
			this.quanity.MinimumWidth = 6;
			this.quanity.Name = "quanity";
			this.quanity.Width = 125;
			// 
			// pay
			// 
			this.pay.HeaderText = "Pay";
			this.pay.MinimumWidth = 6;
			this.pay.Name = "pay";
			this.pay.Width = 125;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.SystemColors.ControlLight;
			this.button1.Location = new System.Drawing.Point(550, 489);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(216, 33);
			this.button1.TabIndex = 12;
			this.button1.Text = "Insert";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.SystemColors.ControlLight;
			this.button2.Location = new System.Drawing.Point(841, 490);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(172, 30);
			this.button2.TabIndex = 13;
			this.button2.Text = "Clear";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.SystemColors.ControlLight;
			this.button3.Location = new System.Drawing.Point(1006, 624);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(121, 31);
			this.button3.TabIndex = 14;
			this.button3.Text = "Close";
			this.button3.UseVisualStyleBackColor = false;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(1181, 674);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.paytext);
			this.Controls.Add(this.quanitycombo);
			this.Controls.Add(this.typecombo);
			this.Controls.Add(this.namecombo);
			this.Controls.Add(this.idtext);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox idtext;
		private System.Windows.Forms.ComboBox namecombo;
		private System.Windows.Forms.ComboBox typecombo;
		private System.Windows.Forms.ComboBox quanitycombo;
		private System.Windows.Forms.TextBox paytext;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.DataGridViewTextBoxColumn id;
		private System.Windows.Forms.DataGridViewTextBoxColumn namecoffee;
		private System.Windows.Forms.DataGridViewTextBoxColumn type;
		private System.Windows.Forms.DataGridViewTextBoxColumn quanity;
		private System.Windows.Forms.DataGridViewTextBoxColumn pay;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
	}
}

