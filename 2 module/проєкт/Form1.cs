﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shop
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (namecombo.SelectedItem.ToString() == "Latte")
			{
				if (typecombo.SelectedItem.ToString() == "Hot")
				{
					paytext.Text = (float.Parse(quanitycombo.Text)*60).ToString();
				}
				if (typecombo.SelectedItem.ToString() == "Ice")
				{
					paytext.Text = (float.Parse(quanitycombo.Text) * 50).ToString();

				}
				dataGridView1.Rows.Add(idtext.Text, namecombo.Text, typecombo.Text, quanitycombo.Text, paytext.Text );
			}

			else if (namecombo.SelectedItem.ToString() == "Chocolatte")
			{
				if (typecombo.SelectedItem.ToString() == "Hot")
				{
					paytext.Text = (float.Parse(quanitycombo.Text) * 70).ToString();
				}
				if (typecombo.SelectedItem.ToString() == "Ice")
				{
					paytext.Text = (float.Parse(quanitycombo.Text) * 55).ToString();

				}
				dataGridView1.Rows.Add(idtext.Text, namecombo.Text, typecombo.Text, quanitycombo.Text, paytext.Text);

			}

			else if (namecombo.SelectedItem.ToString() == "Capuccinno")
			{
				if (typecombo.SelectedItem.ToString() == "Hot")
				{
					paytext.Text = (float.Parse(quanitycombo.Text) * 65).ToString();
				}
				if (typecombo.SelectedItem.ToString() == "Ice")
				{
					paytext.Text = (float.Parse(quanitycombo.Text) * 55).ToString();

				}
				dataGridView1.Rows.Add(idtext.Text, namecombo.Text, typecombo.Text, quanitycombo.Text, paytext.Text);

			}

			else if (namecombo.SelectedItem.ToString() == "Coffee")
			{
				if (typecombo.SelectedItem.ToString() == "Hot")
				{
					paytext.Text = (float.Parse(quanitycombo.Text) * 40).ToString();
				}
				if (typecombo.SelectedItem.ToString() == "Ice")
				{
					paytext.Text = (float.Parse(quanitycombo.Text) * 50).ToString();

				}
				dataGridView1.Rows.Add(idtext.Text, namecombo.Text, typecombo.Text, quanitycombo.Text, paytext.Text);

			}

			else if (namecombo.SelectedItem.ToString() == "Tea")
			{
				if (typecombo.SelectedItem.ToString() == "Hot")
				{
					paytext.Text = (float.Parse(quanitycombo.Text) * 30).ToString();
				}
				if (typecombo.SelectedItem.ToString() == "Ice")
				{
					paytext.Text = (float.Parse(quanitycombo.Text) * 25).ToString();

				}
				dataGridView1.Rows.Add(idtext.Text, namecombo.Text, typecombo.Text, quanitycombo.Text, paytext.Text);

			}
		}

		private void idtext_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (char.IsNumber(e.KeyChar))
			{

			}
			else
			{
				e.Handled = e.KeyChar != (char)Keys.Back;

			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			idtext.Text = "";
			namecombo.Text = "";
			typecombo.Text = "";
			quanitycombo.Text = "";
			paytext.Text = "";
		}

		private void button3_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void idtext_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
